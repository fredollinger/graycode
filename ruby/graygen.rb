#!/usr/bin/env ruby

# Generate graycode test files

require_relative 'GrayBinary'

# Generate binary and decimal file
def get_range()
    (0..255).each do |n|
        gray = GrayBinary.new
        gray.from_decimal(n)
        yield gray, n
    end
end

def gen_decimal()
    fn = File.new("../test/binary2decimal.txt", 'w')
    get_range {|gray, n|
        fn.puts "#{gray.to_s},#{n}"
    }
end

def gen_hex()
    fn = File.new("../test/binary2hex.txt", 'w')
    get_range {|gray, n|
        fn.puts "#{gray.to_s},#{gray.hex}"
    }
end

def gen_gray()
    fn = File.new("../test/binary2gray.txt", 'w')
    get_range {|x, n|
        fn.puts "#{x.to_s},#{x.gray}"
    }
end

# uncomment when done
#gen_decimal
#gen_hex
gen_gray
