#!/usr/bin/ruby

require "minitest/autorun"
require_relative 'GrayBinary.rb'

class TestMeme < Minitest::Test

    def setup
        @gray1 = GrayBinary.new "11111111"
        @gray2 = GrayBinary.new "00000000"
        @gray3 = GrayBinary.new "00000000"
    end

    def testfile (fn)
        File.open(fn, "r") do |file_handle|
            file_handle.each_line do |line|
                res=line.split(',')
            yield res[0], res[1]
            end
        end
    end

    #####################################################################
    # Constructor functions

    # Given a binary string, we can initialize the GrayBinary as that
    # 2.3.1
    def test_from_constructor
        testfile ("../test/binary2decimal.txt") {|one, two|
            gray1 = GrayBinary.new one
            assert_equal one, gray1.to_s
        }
    end

    # 2.3.2
    def test_from_decimal
        testfile ("../test/binary2decimal.txt") {|one, two|
        @gray1.from_decimal two.to_i
            assert_equal @gray1.to_s, one
        }
    end

    # 2.3.3
    def test_from_hex
        testfile ("../test/binary2hex.txt") {|one, two|
        @gray1.from_hex two.chomp
            assert_equal @gray1.to_s, one
        }
    end

    # 2.3.4
    def test_from_gray
        testfile ("../test/binary2gray.txt") {|one, two|
	    @gray1.from_gray one.chomp
	    assert_equal two.chomp, @gray1.to_s
        }
    end

    #####################################################################
    # Operators

    # 2.2.1
    def test_left_shift
        @gray1.from_hex "FF"
        @gray1 << 1
        assert_equal "11111110", @gray1.to_s

        @gray1.from_hex "FF"
        @gray1 << 4
        assert_equal "11110000", @gray1.to_s
    end

    # 2.2.2
    def test_right_shift
        @gray1.from_hex "FF"
        @gray1 >> 1
        assert_equal "01111111", @gray1.to_s

        @gray1.from_hex "FF"
        @gray1 >> 4
        assert_equal "00001111", @gray1.to_s
    end

    # 2.2.3
    def test_xor
        @gray1.from_hex "FF"
        @gray2.from_hex "00"
        @gray3 = @gray1 ^ @gray2
        assert_equal "11111111", @gray3.to_s

        @gray1.from_hex "00"
        @gray2.from_hex "00"
        @gray3 = @gray1 ^ @gray2
        assert_equal "00000000", @gray3.to_s

        @gray1.from_hex "FF"
        @gray2.from_hex "FF"
        @gray3 = @gray1 ^ @gray2
        assert_equal "00000000", @gray3.to_s
    end

    # 2.2.4
    def test_and
        @gray1.from_hex "FF"
        @gray2.from_hex "00"
        @gray3 = @gray1 & @gray2
        assert_equal "00000000", @gray3.to_s

        @gray1.from_hex "00"
        @gray2.from_hex "00"
        @gray3 = @gray1 & @gray2
        assert_equal "00000000", @gray3.to_s

        @gray1.from_hex "FF"
        @gray2.from_hex "FF"
        @gray3 = @gray1 & @gray2
        assert_equal "11111111", @gray3.to_s
    end

    # 2.2.5
    def test_or
        @gray1.from_hex "FF"
        @gray2.from_hex "00"
        @gray3 = @gray1 | @gray2
        assert_equal "11111111", @gray3.to_s

        @gray1.from_hex "00"
        @gray2.from_hex "00"
        @gray3 = @gray1 | @gray2
        assert_equal "00000000", @gray3.to_s

        @gray1.from_hex "FF"
        @gray2.from_hex "FF"
        @gray3 = @gray1 | @gray2
        assert_equal "11111111", @gray3.to_s
    end

    #####################################################################
    # Conversion Functions

    # 2.4.1
    def test_string
        testfile ("../test/binary2decimal.txt") {|one, two|
            gray1 = GrayBinary.new one
            assert_equal one.chomp, gray1.to_s
        }
    end

    # 2.4.2
    def test_decimal
        testfile ("../test/binary2decimal.txt") {|one, two|
            gray1 = GrayBinary.new one
            assert_equal two.to_i, gray1.decimal
        }
    end

    # 2.4.3
    def test_hex
        testfile ("../test/binary2hex.txt") {|one, two|
            gray1 = GrayBinary.new one
            assert_equal two.chomp, gray1.hex
        }
    end

    # 2.4.4
    def test_gray
        testfile ("../test/binary2gray.txt") {|one, two|
            gray1 = GrayBinary.new one
            assert_equal two.chomp, gray1.gray
        }
    end

end
