#!/usr/bin/ruby

# class GrayBinary Encapsulates a binary number
class GrayBinary
    attr_accessor :value

    #####################################################################
    # Constructor functions

    def initialize (k="0")
        @value = k
        @hexmap = {
                "0" => "0000",
                "1" => "0001",
                "2" => "0010",
                "3" => "0011",
                "4" => "0100",
                "5" => "0101",
                "6" => "0110",
                "7" => "0111",
                "8" => "1000",
                "9" => "1001",
                "A" => "1010",
                "B" => "1011",
                "C" => "1100",
                "D" => "1101",
                "E" => "1110",
                "F" => "1111"
        }
        pad_zeros
    end # initialize GrayBinary

    def from_decimal (dec)
        if dec < 2 then
            @value = dec.to_s
            return
        end

        @value = ""

        loop do
            mod = dec % 2
                quot = dec / 2
            if  0 == mod
            then
                @value.prepend("0")
            else
                @value.prepend("1")
            end

            dec = quot
            break if 1 == quot
        end

        @value.prepend("1")

        pad_zeros
    end # from_decimal

    def from_hex (hex)
        @value = ""
        k=hex.split('')
        k.each do |i|
            @value = @value + @hexmap[i]
        end
        pad_zeros
    end

    def from_s (k)
        @value = k
        pad_zeros
    end

    def from_binary (k)
        from_s k
    end

    # TODO
    def from_gray (k)
        puts "TODO: STUB from_gray()"
	mask = 0
	gray1 = GrayBinary.new self.to_s
	loop do
	    gray1 >> 1
	    mask = gray1.decimal
	    puts "mask [#{mask}]"
            break if 0 == mask
	    k = self ^ gray1
	    puts "k [#{k}]"
        end
    end

    #####################################################################
    # Operators functions

    # 2.2.1
    def <<(x)
        (1..x).each do |n|
            @value=@value[1, @value.size] + "0"
        end
    end

    # 2.2.2
    def >>(x)
        res=""
        (1..x).each do |n|
            # puts n
            res = "0" + res
        end

        (0..(@value.size-x-1)).each do |n|
            res = res + @value[n]
        end

        @value = res
    end

    # 2.2.3
    def ^(x)
        res = ''
        grays1=x.to_s.split('')
        grays2=@value.to_s.split('')
        count = 0

        grays1.each do |gray1|
            gray2 = grays2[count]

            if "0" == gray1 and "0" == gray2 then
                res = res + "0"
            elsif "0" == gray1 and "1" == gray2 then
                res = res + "1"
            elsif "1" == gray1 and "0" == gray2 then
                res = res + "1"
            else
                res = res + "0"
            end

            count = count + 1
        end

        resgray = GrayBinary.new res
        return resgray
    end # def ^(x)

    # 2.2.4
    def &(x)
        res = ''
        grays1=x.to_s.split('')
        grays2=@value.to_s.split('')
        count = 0

        grays1.each do |gray1|
            gray2 = grays2[count]
            if "0" == gray1 and "0" == gray2 then
                res = res + "0"
            elsif "0" == gray1 and "1" == gray2 then
                res = res + "0"
            elsif "1" == gray1 and "0" == gray2 then
                res = res + "0"
            else
                res = res + "1"
            end
            count = count + 1
        end

        resgray = GrayBinary.new res
    return resgray
    end # def &(x)

    # 2.2.5
    def |(x)
        res = ''
        grays1=x.to_s.split('')
        grays2=@value.to_s.split('')
        count = 0

        grays1.each do |gray1|
            gray2 = grays2[count]

            if "0" == gray1 and "0" == gray2 then
                res = res + "0"
            elsif "0" == gray1 and "1" == gray2 then
                res = res + "1"
            elsif "1" == gray1 and "0" == gray2 then
                res = res + "1"
            else
                res = res + "1"
            end

            count = count + 1
        end

    resgray = GrayBinary.new res
    return resgray
    end # def |(x)

    #####################################################################
    # Utility functions

    # 2.4.1
    def to_s()
        pad_zeros
        return @value
    end

    # 2.5.1
    def pad_zeros()
        if @value.length > 8 then
            @value.truncate(8)
        end

        zeros = 8-@value.length
        @value = ("0" * zeros ) + @value
    end

    #####################################################################
    # Conversion functions

    # 2.4.2
    def decimal ()
        k=@value.reverse.split('')
        c=0
        res=0
        k.each do |i|
            if 1 == i.to_i then
                res=res+(2 ** c)
            end
            c=c+1
        end
        return res
    end

    # 2.4.3
    def hex()
        pad_zeros
        k1=@value[0,4]
        k2=@value[4,8]
        revmap = @hexmap.invert
        res="#{revmap[k1]}#{revmap[k2]}"
    end

    # 2.4.4
    def gray()
        pad_zeros
	gray1 = GrayBinary.new self.to_s
	gray1 >> 1
	k = self ^ gray1
	return k.to_s
    end

end # GrayBinary
