/* Gray Code
 *
 * Trying to learn rust by manipulating binary.
 *
 * Convert to decimal
 *
 * Convert to graycode
 *
 */

#[allow(dead_code)]
#[derive(PartialEq, PartialOrd)]
#[derive(Debug)]
struct Binary {
    value: String
}

fn to_decimal(msg: &str) -> i64 {
    let s: String = String::from(msg);
    let mut res = 0;
    for (i,c) in s.chars().rev().enumerate() {
        let two: i64 = 2;
        let tmp = i as u32;
        if c == '1' {
            //println!("The number is {}", c);
            res = res + two.pow(tmp)
        }
    }
    return res;
}

#[allow(dead_code)]
fn to_decimal_from_binary(b: Binary) -> i64 {
    //println!("The number is {}", b.value);
    let mut res = 0;
    for (i,c) in b.value.chars().rev().enumerate() {
        let two: i64 = 2;
        let tmp = i as u32;
        if c == '1' {
            //println!("The number is {}", c);
            res = res + two.pow(tmp)
        }
    }
    return res;
}

// IN PROGRESS
#[allow(dead_code)]
fn to_binary_from_decimal(i: i64) -> Binary {
    let b: Binary;
    let mut str = String::from("");
    let mut dec = i;

    if i < 2 {
        b = Binary { value: i.to_string() };
        return b;
    }

    loop {

        let remainder = dec % 2;

        // println!("remainder {}", remainder);

        if 0 == remainder {
            str = format!("{}{}", "0", str);
        }
        else {
            str = format!("{}{}", "1", str);
        }

        let quot = dec / 2;
        // println!("quotient {}", quot);

        if 1 == quot {
            break;
        }

        dec = quot;
    }

    str = format!("{}{}", "1", str);
    b = Binary { value: str };
    return b;
}

#[allow(dead_code)]
fn pad_zeros(b: Binary) -> Binary {
    //let size = ((b.value.len() as f32 / 8 as f32) as i32);
    // TODO NEED TO REMOVE FRACTION BEFORE WE SUBTRACT
    let junk = ((b.value.len() / 8) + 1) * 8;
    // println!("val: is {}", junk);
    let size = junk - b.value.len();

    println!("The size is {}", size);
    let mut res = Binary { value: String::from("") };
    res.value = format!("{}", b.value);

    let mut x = 0;
    for x in 0..size {
        res.value = format!("0{}", res.value);
    }

    println!("mut s is {}", res.value);

    return b;
}

#[test]
fn test_pad_zeros() {
    let a = Binary { value: String::from("00000101") };
    let b = Binary { value: String::from("101") };
    let c: Binary = pad_zeros(b);
    assert_eq!(a, c);
}

#[test]
fn test_binary_from_decimal() {
    // Trivial cases 0 and 1
    let a = to_binary_from_decimal(0);
    let b = Binary { value: String::from("0") };
    assert!(a == b);

    let a = to_binary_from_decimal(1);
    let b = Binary { value: String::from("1") };
    assert!(a == b);

    // currently fails
    let a = to_binary_from_decimal(2);
    let b = Binary { value: String::from("10") };
    assert!(a == b);

}

#[test]
fn test_do_decimal() {
    let y: i64 = to_decimal("101");
    assert_eq!(y, 5);
}

#[test]
fn test_do_decimal_from_binary() {
    let b = Binary { value: String::from("101") };
    let y: i64 = to_decimal_from_binary(b);
    assert_eq!(y, 5);
}

// test_compare: see if we can compare two Binary structs
#[test]
fn test_compare() {
    let a = Binary { value: String::from("101") };
    let b = Binary { value: String::from("101") };
    let c = Binary { value: String::from("111") };
    assert!(a == b);
    assert!(a != c);
}

fn main() {
    //let y: i64 = to_decimal("101");
    //println!("Result is {}", y);
    //let a = Binary { value: String::from("000101") };
    let a = Binary { value: String::from("1111111111111100") };
    //let a = to_binary_from_decimal(6);
    let b = pad_zeros(a);
    //println!("The number is {}", b.value);
}
